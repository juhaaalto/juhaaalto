#!/usr/bin/env python3
"""This script gets daily stock prices from OMXH."""
# (c) copyright 2020 Juha Aalto
from datetime import date
import os
import shutil
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException

def get_a_file(browser, which_list):
    '''Fetches a list of stocks and saves them in csv format to the hard drive.'''
    today = date.today()
    filename = 'osakelista_OMXH_'+which_list+'_'+str(today)+'.csv'

    browser.get("https://www.kauppalehti.fi/porssi/kurssit/"+which_list)
    browser.implicitly_wait(10)
    try:
        browser.find_element(By.XPATH, '//*[@data-qa="oil-YesButton"]').click()
        browser.find_element(By.XPATH, '//*[@aria-labelledby="alma-data-policy-banner__evasteet"]').click()
    except NoSuchElementException:
        print("Popup already clicked.")
    browser.find_element(By.XPATH, '//button[text()="Lataa excel-taulukko"]').click()
    return filename

def clean_tmp():
    '''Clean price files from /tmp.'''
    if os.path.exists("/tmp/osakelista_OMXH.csv"):
        os.remove("/tmp/osakelista_OMXH.csv")
    if os.path.exists("/tmp/osakelista_FNFI.csv"):
        os.remove("/tmp/osakelista_FNFI.csv")

def main():
    '''Main method.'''
    clean_tmp() # remove old files from /tmp
    # To prevent download dialog
    with Display() as display:
        binary = '/usr/local/bin/geckodriver'
        display = Display(visible=0, size=(1024, 768))
        display.start()
        profile = webdriver.FirefoxProfile()
        profile.set_preference('browser.download.folderList', 2)  # custom location
        profile.set_preference('browser.download.manager.showWhenStarting', False)
        profile.set_preference('browser.download.dir', '/tmp')
        profile.set_preference('browser.helperApps.neverAsk.saveToDisk', 'text/csv')
        browser = webdriver.Firefox(profile, executable_path=binary)
        filename_xhel = get_a_file(browser, 'XHEL')
        filename_fnfi = get_a_file(browser, 'FNFI')
        shutil.copy('/tmp/osakelista_OMXH.csv', '/hex/www.hex.com/suomi/markkinainfo/kauppalehti/' + filename_xhel)
        shutil.copy('/tmp/osakelista_FNFI.csv', '/hex/www.hex.com/suomi/markkinainfo/kauppalehti/' + filename_fnfi)
        clean_tmp()
        browser.quit()
        display.stop()


if __name__ == '__main__':
    main()
