'''
(c) 2020-09-05 Juha Aalto

Selects locations in a list. Lists all combinations.
'''
import time

start_time = time.perf_counter()


def location(lista, counter):
    '''
    Prints the location of each "1".
    :param lista:   list of ones and zeros
           counter: counter

    '''
    locations = []
    for k, lis in enumerate(lista):
        if lis == 1:
            locations.append(k)
    print("Time {}s, Counter: {} Locations: {}".format(int(time.perf_counter()-start_time), counter, locations))

def main():
    '''
    Main method.
    '''
    counter = 0
    lista = [1] * 8 + [0] * 22
    print(lista)
    j = len(lista)
    while j > 1:
        j -= 1
        if lista[j-1] == 1 and lista[j] == 0:
            counter += 1
            lista[j] = 1
            lista[j-1] = 0
            queue_size = sum(lista[j:])-1
            ones = queue_size * [1]
            zeros = (len(lista)-j-queue_size-1)*[0]
            lista = lista[:j]+[1]+ones+zeros
            location(lista, counter)
            #print("Time {}s, Counter: {} New List: {}".format(int(time.perf_counter()-start_time), counter, lista))
            j = len(lista)

if __name__ == '__main__':
    main()
