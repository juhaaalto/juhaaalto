#!/usr/bin/env python3
"""
(c) Copyright 2020 Juha Aalto
Fetches data from stat.fi in csv format. Stores data in a mysql table in marketdb database.
This script does not fetch env variables correctly if run from PyCharm. Run from terminal.
"""
import re
from io import StringIO
import os
import requests
import pandas as pd
import mysql.connector
from mysql.connector import Error

class Statfi():
    '''Class to fetch data from stat.fi and store the data in your MySQL database. '''
    def __init__(self, url, json_query):
        self.url = url
        self.json = json_query
        self.month = 12
        self.year = 2200
        self.data = pd.DataFrame()
        self.dataframe = pd.DataFrame()
        self.last_value = float()

    def get_data(self):
        '''Get_data reads data from stat.fi.'''
        response = requests.post(self.url, json=self.json)
        data = response.text
        data = re.sub(r'(\.\d)', r'\1\n', data)
        self.data = StringIO(data)
        self.dataframe = pd.read_csv(self.data, index_col=0, header=None)
        self.last_value = self.dataframe.iloc[-1, 0]
        self.year = re.sub(r'^(\d\d\d\d).*', r'\1', self.dataframe.index[-1])
        self.month = re.sub(r'.*(\d\d)$', r'\1', self.dataframe.index[-1])

    def print_data(self):
        '''Prints the fetched data and the latest value.'''
        print(self.dataframe, self.last_value)

    def update_cci_hex_mo(self, table_name, column_name):
        '''Updates mysql table according to data.'''
        conf = {'host': os.environ['SQL_HOST'],
                'user': os.environ['SQL_USER'],
                'password': os.environ['SQL_PWD'],
                'database': os.environ['SQL_DB']}
        try:
            connection = mysql.connector.connect(**conf)
            mysql_update_query = """UPDATE %s SET %s=%s WHERE month=%s and year=%s""" % (table_name, column_name, "%s", "%s", "%s")
            values = (self.last_value, self.month, self.year)
            cursor = connection.cursor()
            cursor.execute(mysql_update_query, values)
            print("Data updated successfully to MySQL.")
        except Error as error:
            print("Failed to create table in MySQL: {}".format(error))
        finally:
            if  connection.is_connected():
                cursor.close()
                connection.close()
                print("MySQL connection is closed.")

def main():
    '''Main method.'''
    url = 'http://pxnet2.stat.fi/PXWeb/api/v1/en/StatFin/tul/kbar/statfin_kbar_pxt_11cc.px'
    json_query = {"query":[{"code":"Tiedot", "selection": {"filter": "item", "values": ["CCI_A1"]}}], "response": {"format": "csv"}}
    table_name = 'hex_mo'
    column_name = 'CCI_New'
    i = Statfi(url, json_query)
    i.get_data()
    i.update_cci_hex_mo(table_name, column_name)

if __name__ == '__main__':
    main()
