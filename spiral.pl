#! /usr/local/bin/perl 
# (c) 22.04.2001 Juha Aalto.

use Getopt::Std;
use POSIX;
use DBI;
use Date::Business;
use diagnostics;

my @days  = ( "19771125", "19731017", "19890407", "19871017", "19880110" );
my $user  = "juha";
my $pass  = "juhapass";
my $usage = "usage: $0 [ -nnumber ]\n\n\"number\" is greater than one.\n\n";
my $filter;
my $turning_point_filter = 1.1;

getopts('n:') or die print $usage;
if ($opt_n) {
    $filter = $opt_n;
}
else { die print $usage; }
$dbh = DBI->connect( "dbi:mysql:marketdb", $user, $pass, { RaiseError => 1 } )
  or die "Couldn't connect to database: " . DBI->errstr;
print "<BODY BGCOLOR=white><TABLE BORDER=1>\n";
make_chart();
print "</TABLE>\n";
$dbh->disconnect;
exit(0);

###########################################################
###########################################################
sub make_chart {
    my @x;
    my @y;
    my @z;
    my @data;
    my @all_data;
    my ( $y_min, $y_max );
    $sth = $dbh->prepare("SELECT date,hex from hse_index order by 1");
    $sth->execute();
    while ( @data = $sth->fetchrow_array() ) {
        if ( $data[0] && $data[1] ) {
            $row = "$data[0] $data[1]";
            push @tmp, $row;
        }
    }
    @tmp = sort @tmp;
    my ( @juha, @turning_points ) = extreme( \@tmp );

    foreach $row (@days) {
        my @spiral_dates = date($row);
        $i = 6;
        foreach (@spiral_dates) {
            s/^(.*)$/$1 $i $row   /;
            $i++;
        }
        push @juha, @spiral_dates, @turning_points;
    }
    $i = 0;
    @juha = sort @juha;
    foreach $row (@juha) {
        if ( $row =~ /top/ ) {
            my $major_tp = $row;
            $major_tp =~ s/^\d+ (\d+) .*/$1/;
            if ( $major_tp > 19 ) {
                $row =~
s/ /<\/TD><TD bgcolor=#ffcc99><font size=-1 color="blue">&nbsp;/g;
                $row =~
                  s/^/<TR><TD bgcolor=#ffcc99><font size=-1 color="blue">/;
            }
            else {
                $row =~ s/ /<\/TD><TD><font  size=-1 color="blue">&nbsp;/g;
                $row =~ s/^/<TR><TD><font size=-1 color="blue">/;
            }
        }

        elsif ( $row =~ /bottom/ ) {
            my $major_tp = $row;
            $major_tp =~ s/^\d+ (\d+) .*/$1/;
            if ( $major_tp > 19 ) {
                $row =~
s/ /<\/TD><TD bgcolor=#ffcc99><font  size=-1 color="red">&nbsp;/g;
                $row =~ s/^/<TR><TD bgcolor=#ffcc99><font size=-1 color="red">/;
            }
            else {
                $row =~ s/ /<\/TD><TD><font  size=-1 color="red">&nbsp;/g;
                $row =~ s/^/<TR><TD><font size=-1 color="red">/;
            }
        }
        elsif ( $row =~ /high/ ) {
            $row =~ s/ /<\/TD><TD><font size=-1 color="green"><B>&nbsp;/g;
            $row =~ s/^/<TR><TD><font size=-1 color="green"><B>/;
        }
        elsif ( $row =~ /low/ ) {
            $row =~ s/ /<\/TD><TD><font size=-1 color="black"><B>&nbsp;/g;
            $row =~ s/^/<TR><TD><font size=-1 color="black"><B>/;
        }
        else {
            $row =~ s/ /<\/TD><TD>/g;
            $row =~ s/^/<TR><TD><font size=-1>/;
        }
    }

    foreach my $row (@juha) {
        $row =~ s/$/<\/TD><\/TR>/;
        print "$row\n";
    }
}

##########################################################
sub extreme {
    my $xyz = shift;
    my @tmp2;
    @data = @$xyz;
    my @turning_points;
    foreach $row (@data) {
        ( my $time, my $price ) = split( / /, $row );
        push @dates,  $time;
        push @prices, $price;
    }

    for ( $i = 1 ; $i < $#prices ; $i++ ) {
        $max_pre = $prices[$i];
        $min_pre = $prices[$i];
        $max_post = $prices[$i];
        $min_post = $prices[$i];
        if (   $prices[ $i - 1 ] < $prices[$i]
            && $prices[ $i + 1 ] < $prices[$i] )
        {
            for ( $j = $i - 1 ; $j >= 0 ; $j-- ) {
                $min_pre = $prices[$j] if ( $prices[$j] < $min_pre );
                last if $prices[$j] >= $prices[$i];
            }
            for ( $j = $i + 1 ; $j <= $#prices ; $j++ ) {
                $min_post = $prices[$j] if ( $prices[$j] < $min_post );
                last if $prices[$j] >= $prices[$i];
            }
            if ( $min_pre < $min_post ) {
                my $result       = $prices[$i] / $min_post;
                my @spiral_dates = date( $dates[$i] );
                if ( $result >= $turning_point_filter ) {
                    push @turning_points,
                      "$dates[$i]   $prices[$i] $result high";
                }
                for ( $j = 0 ; $j <= $#spiral_dates ; $j++ ) {
                    my $spiral_no = $j + 6;
                    if ( $result >= $filter ) {
                        push @tmp2,
"$spiral_dates[$j] $spiral_no $dates[$i] $prices[$i] $result top";
                    }
                }
            }
            else {
                my $result       = $prices[$i] / $min_pre;
                my @spiral_dates = date( $dates[$i] );
                if ( $result >= $turning_point_filter ) {
                    push @turning_points,
                      "$dates[$i]   $prices[$i] $result high";
                }
                for ( $j = 0 ; $j <= $#spiral_dates ; $j++ ) {
                    my $spiral_no = $j + 6;
                    if ( $result >= $filter ) {
                        push @tmp2,
"$spiral_dates[$j] $spiral_no $dates[$i] $prices[$i] $result top";
                    }
                }
            }
        }
        elsif ($prices[ $i - 1 ] > $prices[$i]
            && $prices[ $i + 1 ] > $prices[$i] )
        {
            for ( $j = $i - 1 ; $j >= 0 ; $j-- ) {
                $max_pre = $prices[$j] if ( $prices[$j] > $max_pre );
                last if $prices[$j] <= $prices[$i];
            }
            for ( $j = $i + 1 ; $j <= $#prices ; $j++ ) {
                $max_post = $prices[$j] if ( $prices[$j] > $max_post );
                last if $prices[$j] <= $prices[$i];
            }
            if ( $max_pre > $max_post ) {
                my $result       = $max_post / $prices[$i];
                my @spiral_dates = date( $dates[$i] );
                if ( $result >= $turning_point_filter ) {
                    push @turning_points,
                      "$dates[$i]   $prices[$i] $result low";
                }
                for ( $j = 0 ; $j <= $#spiral_dates ; $j++ ) {
                    my $spiral_no = $j + 6;
                    if ( $result >= $filter ) {
                        push @tmp2,
"$spiral_dates[$j] $spiral_no $dates[$i] $prices[$i] $result bottom";
                    }
                }
            }
            else {
                my $result       = $max_pre / $prices[$i];
                my @spiral_dates = date( $dates[$i] );
                if ( $result >= $turning_point_filter ) {
                    push @turning_points,
                      "$dates[$i]   $prices[$i] $result low";
                }
                for ( $j = 0 ; $j <= $#spiral_dates ; $j++ ) {
                    my $spiral_no = $j + 6;
                    if ( $result >= $filter ) {
                        push @tmp2,
"$spiral_dates[$j] $spiral_no $dates[$i] $prices[$i] $result bottom";
                    }
                }
            }
        }

    }
    return @tmp2, @turning_points;
}

sub date {
    my $spiral_date = shift;
    my @tmp;
    $d1 = new Date::Business( DATE => $spiral_date );
    $d1->add(84);
    push @tmp, $d1->image();
    $d2 = new Date::Business( DATE => $spiral_date );
    $d2->add(107);
    push @tmp, $d2->image();
    $d3 = new Date::Business( DATE => $spiral_date );
    $d3->add(135);
    push @tmp, $d3->image();
    $d4 = new Date::Business( DATE => $spiral_date );
    $d4->add(172);
    push @tmp, $d4->image();
    $d5 = new Date::Business( DATE => $spiral_date );
    $d5->add(219);
    push @tmp, $d5->image();
    $d6 = new Date::Business( DATE => $spiral_date );
    $d6->add(278);
    push @tmp, $d6->image();
    $d7 = new Date::Business( DATE => $spiral_date );
    $d7->add(354);
    push @tmp, $d7->image();
    $d8 = new Date::Business( DATE => $spiral_date );
    $d8->add(451);
    push @tmp, $d8->image();
    $d9 = new Date::Business( DATE => $spiral_date );
    $d9->add(573);
    push @tmp, $d9->image();
    $d10 = new Date::Business( DATE => $spiral_date );
    $d10->add(729);
    push @tmp, $d10->image();
    $d11 = new Date::Business( DATE => $spiral_date );
    $d11->add(928);
    push @tmp, $d11->image();
    $d12 = new Date::Business( DATE => $spiral_date );
    $d12->add(1180);
    push @tmp, $d12->image();
    $d13 = new Date::Business( DATE => $spiral_date );
    $d13->add(1501);
    push @tmp, $d13->image();
    $d14 = new Date::Business( DATE => $spiral_date );
    $d14->add(1910);
    push @tmp, $d14->image();
    $d15 = new Date::Business( DATE => $spiral_date );
    $d15->add(2429);
    push @tmp, $d15->image();
    $d16 = new Date::Business( DATE => $spiral_date );
    $d16->add(3090);
    push @tmp, $d16->image();
    $d17 = new Date::Business( DATE => $spiral_date );
    $d17->add(3930);
    push @tmp, $d17->image();
    $d18 = new Date::Business( DATE => $spiral_date );
    $d18->add(4999);
    push @tmp, $d18->image();
    $d19 = new Date::Business( DATE => $spiral_date );
    $d19->add(6359);
    push @tmp, $d19->image();
    $d20 = new Date::Business( DATE => $spiral_date );
    $d20->add(8089);
    push @tmp, $d20->image();
    $d21 = new Date::Business( DATE => $spiral_date );
    $d21->add(10289);
    push @tmp, $d21->image();
    $d22 = new Date::Business( DATE => $spiral_date );
    $d22->add(13088);
    push @tmp, $d22->image();
    $d23 = new Date::Business( DATE => $spiral_date );
    $d23->add(16648);
    push @tmp, $d23->image();
    $d24 = new Date::Business( DATE => $spiral_date );
    $d24->add(21176);
    push @tmp, $d24->image();
    $d25 = new Date::Business( DATE => $spiral_date );
    $d25->add(26937);
    push @tmp, $d25->image();
    $d26 = new Date::Business( DATE => $spiral_date );
    $d26->add(34264);
    push @tmp, $d26->image();
    $d27 = new Date::Business( DATE => $spiral_date );
    $d27->add(43585);
    push @tmp, $d27->image();
    $d28 = new Date::Business( DATE => $spiral_date );
    $d28->add(55440);
    push @tmp, $d28->image();
    $d29 = new Date::Business( DATE => $spiral_date );
    $d29->add(70521);
    push @tmp, $d29->image();
    $d30 = new Date::Business( DATE => $spiral_date );
    $d30->add(89704);
    push @tmp, $d30->image();
    $d31 = new Date::Business( DATE => $spiral_date );
    $d31->add(114106);
    push @tmp, $d31->image();
    $d32 = new Date::Business( DATE => $spiral_date );
    $d32->add(145148);
    push @tmp, $d32->image();
    return @tmp;
}

